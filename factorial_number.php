<!Doctype html>
<html>
    <head>
        <title>Code for factorial of number</title>
    </head>
    <body>
        <form>
            <fieldset>
                <legend>Factorial of number</legend>
                Enter Number : <input type="text" name="number" id="number"><br>
                <input type="submit" value="calculate">
            </fieldset>
        </form>
    </body>
</html>
<?php
    if(isset($_REQUEST['number'])){
        $num = $_REQUEST['number'];
        $fact = 1;
        for($x = $num; $x >=1; $x--){

            $fact = $fact * $x;
        }
        echo "Factorial of entered number is " . $fact;
    }
?>