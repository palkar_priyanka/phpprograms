<?php
/*
php engine creates a logical object to preserve data across subsequent HTTP request which is known as session.
sessions generally stores temporary data.
simply it maintain data of an user (browser).
session variables are set with the PHP global variable $_SESSION.
stored in temp folder(server)

session_start() method is used to start the session on the php page.

<?php
print_r($_SESSION);
?>

to remove all global session variables and destroy the session use session_unset() and session_destroy() 
*/
	session_start(); 
?>
<!DOCTYPE html>
<html>
	<body>
		<?php
			$_SESSION["username"] = "priyanka";
			echo "session variable is set to" . " " . $_SESSION["username"];
			
			session_unset();
			session_destroy();
			echo $_SESSION["username"]; // 
		?>
	</body>
</html>