<?php
/*
diff include and require
-Require and include both are used to include a file, but if file is not found
	include sends a waring error and execute the script and
	require sends fatal error halt the script.
	
	syntax :
	
	include 'filename';
	require 'filename';
*/
?>
<!DOCTYPE html>
<html>
	<body>
		<h1>Welcome to my home page..!</h1>
		<p>Some text</p>
		<p>Some more text</p>
		<?php include 'footer.php' ?>
		
		<h1>Welcome to my home page..!</h1>
		<?php require 'nofileexit.php' ?>
		echo "I have a " . $color . $car;
	</body>
</html>