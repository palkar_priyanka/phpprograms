<?php
	/*
	Cookie -> cookie is often used to identify user.
			A cookie is a small file that the server embeds on the user's computer.
			with php, you can both create and retrive cookie value.
		Cookie is creted using setcookie() function.
		
		syntax :
				setcookie(name,value,expiry);
	*/
	setcookie('user','priyanka',time() + 3600); //time() + 3600 = 1 hour
	
	echo $_COOKIE['user'];	
?>
<!DOCTYPE html>
<html>
	<body>
		<p><a href="http://yahoo.com" onclick="window.open('http://google.com');">Click to open Google and Yahoo
			</a>
		</p>
	</body>
</html>