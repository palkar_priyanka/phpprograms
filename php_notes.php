<?php
	/*What is php ?
Ans - PHP stands for hypertext preprocessor.
	-It is an open source server side scripting language.
	-IT is widely used for web development.
	-It supports many database like Mysql, Orcale, Sybase,etc.
	
	ITS FEATURE - 
	Performance - Script written in php execute much faster than those scripts written in other languages such as JSP and ASP.
	
	Open source software - PHP source code is free availiable on web,you can developed all the versions of php according to 
							your requirement without paying any cost.
	
	Platform Independent - php are available for 
							windows, MAC, Linux, Unix
							A php application developed in one OS can be easily executed in other OS also.
	
	Compatibility - Php is compatible with almost all local servers used today like Apache, IIS etc.
	
	Embedded - PHP code can be easily embedded within HTML tag and scripts.
	
	what is PEAR in php ?
Ans - PEAR is a framework and repository for reusable PHP components.PEAR stands for PHP Extension and Application Reository.
	-It contain all types of php code snippest and libraries.
	
	Diff between static and dynamic websites?
Ans - In Static website,content can't be changed after running the script.you can't change anything in the site. It is predefined.
	- In dynamic website, content of the script can be changed at the run time.Its content is regenerated every time a user visit or 
		reload,Google yahoo and every search engine is the example of dynamic website.
	
	Diff $msg and $$msg ?
Ans - $msg stores variable data while $$msg is used to store variable of variable.
	- $msg stores fixed whereas the data stored in $$msg may be changed dynamically.
	
	Ways to define constant in php ?
Ans - Php constants are name or identifier that cant be changed during the execution of the scripts.
	-define() or Const().
	
	define("greetings","welcome to the php");
	echo greetings;
	
	How many datatypes are there in php ?
Ans - Scalar types,compounds type,special type.
		scalar -> boolean,integer,float,string.
		compounds -> array,object.
		special tpes -> resource , NULL.
	
	Count() function ?
Ans - the php count() function is used to count total elements in the array or something in object.

	Header() function ? 
Ans - the header() function is used to send raw http header to client. It must be called before sending the actual output.

	isset() function ?
Ans - the isset() function checks if the variable is defined and not null.

	PHP array() function ?
Ans - php array() -> php array() functions creates and returns array.
	- php array_change_key_case() -> this function changes the case of all key of an array.
	- php array_chunk() -> this function splits array into chunks.its divide array into small parts.
	- php count() -> counts all elements in the array.
	- php sort() -> sorts all the elements in an array.
	- php array_reverse() -> this function returns array containing elements in reversed order.
	- php array_search() -> searches the specified value in an array. it returns key if search is successfull.
	- php array_intersect() -> it returns the matching elements of two array.
	
	php String function ?
Ans - strtolower() -> returns string in lowercase letter.
	- strtoupper() -> returns string in uppercase letter.
	- ucfirst() -> returns string converting first character into uppercase.IT doesn't change the case of other character.
	- Icfirst() -> returns string converting first character into lowercase . It doesn't change the case of other character.
	- ucwords() -> returns string converting first character of each word into uppercase.
	- strrev() -> returns reversed string.
	- strlen() -> returns length of string.
	
	methods to submit forms in php?
Ans - We can create and use forms in php, To get form data,we need to use PHP superglobals $_GET and $_POST.
	- The form request may be get or post. To retrive data from get request,we need to use $_GET and for post request $_POST.
	
	$_GET -> 
	- Get request is default form request.
	- The data passed through get request is visible on the URL browser so it is not secured.
	- You can send limited amount of data through get request.
	
	$_POST ->
	- Post request is widely used to submit form that have large amount of data such as file upload , image upload, login form,
		registration form etc.
	- The data passed through post request is not visible on the URL browser so it is secured.
	- You can send large amount of data through post request.
	
	How can you submit a form without a submit button?
Ans - we can use javascript submit function to submit form without explicitly clickling any submit button.
	code ->
	document.getElementById('jsform').submit();
	
	What are the ways to include file in PHP?
Ans - PHP allows you to include file so that page content can be reused again. there are two ways to include file in php.
	
	<?php include("filename.php"); ?>
	<?php require("filename.php"); ?>
	
	Differentiate between require and include?
Ans - include() function include a file based in the given path. it gives a warning and allow script to continue if file
	  is not found by include() function.
	- Require() function halt the script and generate fatal error if file is not found by the function.
	
	Explain setcookie() function in PHP ?
Ans - PHP setcookie() function is used to set cookie with HTTP response. Once cookie is set it can access by $_COOKIE
	  superglobal variable.
	code ->
		setcookie("cookie_name","cookie_value","time()+3600");
	we can retrive cookie by $_COOKIE["user"];
	
	What is session ?
Ans - PHP engine creates a logical object to preserve data across a sbsequent HTTP requests,which is known as session.
	- sessions generally stores temporary data.
	- simply it maintain data of an user (browser).
	- session variables are set with the PHP global variable $_SESSION.
	- stored in temp folder(server)
	- session_start() method is used to start the session on the php page.
	- to remove all global session variables and destroy the session use session_unset() and session_destroy()

	<?php
	print_r($_SESSION);
	?>
	
	What is the difference between session and cookie?
Ans - The main difference between session and cookie is that cookies are stored on user's computer in the text file format
	  where as the sessions are stored on server side.
	- Cookies can't hold multiple variables on the other hand session can hold multiple variables.
	- you can manually set an expiry for a cookie, while session is remain active as log as browser is open.
	
	Write syntax to open a file in PHP?
Ans - fopen() function is used to open a file. it accepts two argument : $filename and $mode;
	
	How to read a file in PHP?
Ans - Php provides various function to read data from file. there are different functions that allow you to read all file data,
		read data line by line and read data character by character.
	
	fread() -> used to read data of the file. It requires two arguments:file resource, and file size.
	
	<?php    
		$filename = "c:\\file1.txt";    
		$fp = fopen($filename, "r");//open file in read mode    
		  
		$contents = fread($fp, filesize($filename));//read file    
		  
		echo "<pre>$contents</pre>";//printing data of file  
		fclose($fp);//close file    
	?>  
	
	fgets() -> used to read single line from the file.
	
	<?php    
		$fp = fopen("c:\\file1.txt", "r");//open file in read mode    
		echo fgets($fp);  
		fclose($fp);  
	?>
	
	fgetc() -> used to read single character from the file. to get all data using fgetc() function,use !feof()
		function inside the while loop.
		
	<?php    
		$fp = fopen("c:\\file1.txt", "r");//open file in read mode    
		while(!feof($fp)) {  
		  echo fgetc($fp);  
		}  
		fclose($fp);  
	?> 

	
*/
?>