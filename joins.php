<?php
/*
Joins -> A sql join statement is used to combine data or rows from two or more tables based on a common field bewteen them.

Difference type of joins -> 1. INNER Joins , 2.LEFT JOIN, 3.RIGHT JOIN, 4.FULL JOIN
	
	Inner join -> the inner join keywords selects records that have matching values in both tables.
	
	SELECT Orders.OrderID, Customers.CustomerName
	FROM Orders
	INNER JOIN Customers ON Orders.CustomerID = Customers.CustomerID;
	
	Left join -> the left join keywords returns all records from the left table and matched records from right table.
	
	SELECT Customers.CustomerName, Orders.OrderID
	FROM Customers
	LEFT JOIN Orders ON Customers.CustomerID = Orders.CustomerID
	ORDER BY Customers.CustomerName;
	
	Right join -> the right join keyword returns all the records from right table and matched records from left table.
	
	SELECT Orders.OrderID, Employees.LastName, Employees.FirstName
	FROM Orders
	RIGHT JOIN Employees ON Orders.EmployeeID = Employees.EmployeeID
	ORDER BY Orders.OrderID;
	
	Full outer join -> the full outer join keyword returns all records when there is match in either left or right table records.
	
	SELECT Customers.CustomerName, Orders.OrderID
	FROM Customers
	FULL OUTER JOIN Orders ON Customers.CustomerID=Orders.CustomerID
	ORDER BY Customers.CustomerName;
	
	Self join -> a self join is regular but the table is join with self.
	
	NOT NULL Constraint -> by default a column can hold null values.
		- the NOT NULL constraint enforces  a column to not accept NULL values.
		- This enforces a field to always contain a value, which means that you cannot insert a new record, 
			or update a record without adding a value to this field.
?> 