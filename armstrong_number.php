<!Doctype html>
<html>
    <head>
        <title>Armstrong Number</title>
    </head>
    <body>
        <form>
            <fieldset>
                <legend>Code for armstrong number</legend>
                Enter number : <input type="text" name="number" id="number"></br>
                <input type="submit" value="check">
            </fieldset>
        </form>
    </body>
</html>
<?php
    //An Armstrong number is the one whose value is equal to the sum of the cubes of its digits.
if(isset($_REQUEST['number'])){
   
    $num = $_REQUEST['number'];
    $total = 0;
    $x = $num;
    while ($x!=0){
        $reminder = $x%10;
        $total = $total + $reminder*$reminder*$reminder;
        $x = $x/10;
    }
    if($num == $total){
        echo "Entered number " . $num . " is armstrong number";
    }else{
        echo "Entered number " . $num . " is not armstrong number";
    }
}
?>