<?php
/*	Explode() -> explode function is used to split string into an array.
				 Seperator must be specified.
	$str = "A,B,C,D,F";
	$exp_str = explode(",",$str);
	echo "<pre>";
	print_r($exp_str);
	echo "</pre>";

	implode () -> implode function is used to combine array to make a string.
				the seperator parameter is optional.
*/
$arr = array('hello','world','beautiful','day!');
$imp_str = implode(" ",$arr);
echo $imp_str;

?>